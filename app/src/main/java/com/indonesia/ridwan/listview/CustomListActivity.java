package com.indonesia.ridwan.listview;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.indonesia.ridwan.listview.adapter.MakananAdapter;

/**
 * Created by hasanah on 6/16/16.
 */
public class CustomListActivity extends AppCompatActivity {
    private ListView listView;
    String[] tulisan={"cumi","kakap","kepiting","teri","udang"};
    int[] gambar={R.drawable.cumi,R.drawable.kakap,R.drawable.kepiting,R.drawable.terei,R.drawable.udang};

            @Override
    protected void onCreate(Bundle savedInstanceState)
            {
                super.onCreate(savedInstanceState);
                setContentView(R.layout.activity_main);
                listView=(ListView)findViewById(R.id.listview);

                MakananAdapter adapter=new MakananAdapter(CustomListActivity.this,tulisan,gambar);
                listView.setAdapter(adapter);
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
                {
                    @Override
                    public void onItemClick(AdapterView<?>adapterView, View view, int i, long l)
                    {
                        switch (i)
                        {
                            case 0:
                                Toast.makeText(getApplicationContext(),"Posisi ke 0", Toast.LENGTH_LONG).show();
                                return;
                            case 1:
                                Toast.makeText(getApplicationContext(),"Posisi Ke 1", Toast.LENGTH_LONG).show();
                                return;
                        }
                    }
                });
            }

}
