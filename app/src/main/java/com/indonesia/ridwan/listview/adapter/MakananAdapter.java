package com.indonesia.ridwan.listview.adapter;

import android.app.Activity;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.indonesia.ridwan.listview.R;

/**
 * Created by hasanah on 6/16/1
 */
public class MakananAdapter extends ArrayAdapter <String> {

    private final Activity mContext;
    private String[] mTulisan;
    private int[] mGambar;

    public MakananAdapter(Activity context,String[] tulisan, int[] gambar)
    {
        super(context, R.layout.row_belajar,tulisan);

        mContext=context;
        mGambar=gambar;
        mTulisan=tulisan;
    }

    @Override
    public View getView(int position, View concertView, ViewGroup parent)
    {
        LayoutInflater inflater=mContext.getLayoutInflater();

        View rowView=inflater.inflate(R.layout.row_belajar,null,true);
        ImageView gambar=(ImageView)rowView.findViewById(R.id.gambar);
        TextView judul=(TextView)rowView.findViewById(R.id.ttulisan);


        gambar.setImageResource(mGambar[position]);
        judul.setText(mTulisan[position]);
        return rowView;

    }
}
